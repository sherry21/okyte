<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '2736680a61b8bb543b8e1167db2624f2399111641e41c68f');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x`Z^3yFWtmrav~rI{ 8_m>7xI1)VAvR~0my|-,%zE4}!Jj>(NQFJ;!xBtVB|-UK?');
define('SECURE_AUTH_KEY',  'GM,&2Dw?DXBzQ4Dmaa!s#$PpreA(oFV{Fu-.+16 D^}H>PS=aZY!httk+ +q lvO');
define('LOGGED_IN_KEY',    '5(TUUkh!ZiZ f`n!@Y=iYRQwzJ:RTzLO;]U_{UG5)I(nDRFN~-+=Md+W^}lTayIz');
define('NONCE_KEY',        'ne|`f5psm#BjS3Ff%#==#^L roK&eOB^rXLd; <ofG:7z5:%)]qskdWa<JXirm=d');
define('AUTH_SALT',        'b/4VJ1pw!JK!6Ny:c5M(4mue.=VY@X~!gdp &Qq4y?awmQ_M3dE4^g}/`aJ[5?KI');
define('SECURE_AUTH_SALT', 'Wa,+29s9M+_E:2Rz0sWuG!rn#s4.({ok;O~c+ w>q|#EY1*+;M~_z/y%v>tcLOe$');
define('LOGGED_IN_SALT',   'u-uF2||r.6k1xP3!g[^7E?JF|fSXhTL|![e^{1e1}~u~;i?WO_,Yu|PB2:s-JS(+');
define('NONCE_SALT',       '@CTRO/|+ehQT2PV5*#WgizGfo+mB@YEFa93:K,cNg#$`r`ea4q}|5ABe>c$^pexv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
